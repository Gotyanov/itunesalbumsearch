//
//  ITunesAlbumSearchTests.swift
//  ITunesAlbumSearchTests
//
//  Created by Aleksey Gotyanov on 13.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import XCTest
import RxTest
import RxBlocking
@testable import ITunesAlbumSearch

class ITunesAlbumSearchIntegrationTests: XCTestCase {

    var client: ITunesClient!

    override func setUp() {
        super.setUp()

        client = ITunesClient()
    }
    
    func testAlbumsFetching() throws {
        let result = try client.findAlbum(query: "walls").toBlocking(timeout: 1).single()
        XCTAssert(result.count > 0)
    }

    func testTracksFetching() throws {
        let collectionId = 1151618558
        let tracks = try client.getTracks(forAlbum: collectionId).toBlocking(timeout: 1).single()
        XCTAssert(tracks.count > 0)
    }

}
