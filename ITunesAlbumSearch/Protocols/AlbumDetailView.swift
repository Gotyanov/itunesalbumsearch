//
//  AlbumDetailView.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 15.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import RxSwift

protocol AlbumDetailView: class {
    associatedtype TrackView
    associatedtype TrackViewIndex

    var albumName: String { get set }
    var artistName: String { get set }
    var trackCount: String { get set }
    var genre: String { get set }
    var releaseDate: String { get set }
    var price: String { get set }
    var copyright: String { get set }
    var explicitContent: Bool { get set }
    var artwork: UIImage? { get set }

    func setTrackCount(_: LoadingObject<Int>)

    var tapOnArtistName: Observable<Void> { get }

    var updateTrackViewRequest: Observable<(TrackView, TrackViewIndex)> { get }
}
