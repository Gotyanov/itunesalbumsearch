//
//  Wireframe.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 16.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit
import RxSwift

func createWireframe<Client: AlbumClient>(
    albumClient: Client,
    albumCollectionViewController: AlbumCollectionViewController,
    rootNavController: UINavigationController) -> DisposeBag {

    let disposeBag = DisposeBag()
    let imageCacheManager = ImageCacheManager()
    let albumListInteractor = albumCollectionViewController.createInteractor(client: albumClient)
    var albumDetailInteractor: AlbumDetailInteractor?
    let albumDetailSubject = PublishSubject<AlbumInfo>()

    albumListInteractor.openAlbumDetailsRequest.subscribe(onNext: { album in
        let albumDetailInteractor = albumDetailInteractor ?? {
            let interactor = createAlbumDetailInteractor(
                client: albumClient,
                imageCacheManager: imageCacheManager,
                albumDetailSubject: albumDetailSubject,
                disposeBag: disposeBag)

            albumDetailInteractor = interactor
            return interactor
            }()

        let albumDetailVc = albumDetailInteractor.view
        albumDetailSubject.onNext(album)
        guard !rootNavController.viewControllers.contains(where: { $0 === albumDetailVc }) else { return }
        rootNavController.pushViewController(albumDetailVc, animated: true)
    }).disposed(by: disposeBag)

    albumListInteractor.errorObservable.subscribe(onNext: { error in
        NSLog("error occured: %@", error as NSError)
    }).disposed(by: disposeBag)

    return disposeBag
}

fileprivate func createAlbumDetailInteractor<Client: AlbumClient>(
    client: Client,
    imageCacheManager: ImageCacheManager,
    albumDetailSubject: Observable<AlbumInfo>,
    disposeBag: DisposeBag) -> AlbumDetailInteractor {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    guard let albumDetailVc =
        storyboard.instantiateViewController(withIdentifier: "AlbumDetailTableViewController") as? AlbumDetailTableViewController
        else { fatalError("wrong type of view controller with id AlbumDetailTableViewController") }

    let interactor = albumDetailVc.createInteractor(albumDetailObservable: albumDetailSubject, client: client, imageCacheManager: imageCacheManager)
    interactor.errorObservable.subscribe(onNext: { error in
        NSLog("error occured: %@", error as NSError)
    }).disposed(by: disposeBag)

    return interactor
}
