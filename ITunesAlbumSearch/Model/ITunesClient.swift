//
//  ITunesClient.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 13.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import iTunesSearchAPI
import RxSwift

final class ITunesClient: AlbumClient {

    private let iTunes: iTunes

    init() {
        self.iTunes = iTunesSearchAPI.iTunes()
    }

    fileprivate var currentCountry: Country? {
        let locale = Locale.current
        guard let region = locale.regionCode else { return nil }
        return Country(rawValue: region.lowercased())
    }

    func findAlbum(query: String) -> Observable<[AlbumInfo]> {
        let query = query.trimmingCharacters(in: CharacterSet.whitespaces)
        guard query.count > 0 else { return Observable.of([]) }
        
        return makeSearchRequest(query: query, forMediaType: .music(.album))
            .map(parseAlbums(fromJson: ))
    }

    func getTracks(forAlbum album: Int) -> Observable<[TrackInfo]> {
        return makeLookupRequest(entityId: album, resultMediaType: .music(Entity.musicSong))
            .map(parseTracks(fromJson: ))
    }

    fileprivate func makeSearchRequest(query: String, forMediaType mediaType: Media) -> Observable<Any> {
        let options = Options(country: currentCountry)

        return Observable.create { observer in
            let task = self.iTunes.search(for: query, ofType: mediaType, options: options) { result in
                switch result {
                case .success(let response):
                    observer.onNext(response)
                    observer.onCompleted()
                case .failure(let error):
                    observer.onError(error)
                }
            }

            return Disposables.create {
                task?.cancel()
            }
        }.share()
    }

    fileprivate func makeLookupRequest(entityId: Int, resultMediaType mediaType: Media) -> Observable<Any> {
        let options = Options(country: currentCountry)

        return Observable.create { observer in
            let task = self.iTunes.lookup(by: LookupType.id(entityId.description), entitiesOfType: mediaType, options: options) { result in
                switch result {
                case .success(let response):
                    observer.onNext(response)
                    observer.onCompleted()
                case .failure(let error):
                    observer.onError(error)
                }
            }

            return Disposables.create {
                task?.cancel()
            }
        }.share()
    }
}

fileprivate func parseAlbum(fromJson jsonObject: Any) throws -> AlbumInfo {
    guard let dict = jsonObject as? [String: Any] else { throw MissedPropertyError("result item") }

    func get<T>(_ propertyName: String) throws -> T {
        let result = dict[propertyName] as Any
        guard let value = result as? T else { throw MissedPropertyError(propertyName) }
        return value
    }

    let artistViewUrl = try { () -> URL? in
        guard let urlString = try get("artistViewUrl") as String? else { return nil }
        return URL.init(string: urlString)
    }()

    let collectionViewUrl = try { () -> URL in
        let urlString = try get("collectionViewUrl") as String
        guard let url = URL.init(string: urlString) else { throw MissedPropertyError("collectionViewUrl") }
        return url
    }()

    let artworkUrl = { () -> URL? in
        guard let maybeString = try? (get("artworkUrl100") as String?) ?? (get("artworkUrl60") as String?),
            let exactlyString = maybeString
            else { return nil}

        return URL(string: exactlyString)
    }()

    let releaseDate = try { () -> Date in
        let dateString = try get("releaseDate") as String
        guard let date = ISO8601DateFormatter().date(from: dateString) else { throw MissedPropertyError("releaseDate") }
        return date
    }()

    let collectionPrice = try (get("collectionPrice") as NSNumber?)?.decimalValue

    let album = try AlbumInfo(artistID: get("artistId"),
                          collectionID: get("collectionId"),
                          artistName: get("artistName"),
                          collectionName: get("collectionName"),
                          artistViewUrl: artistViewUrl,
                          collectionViewUrl: collectionViewUrl,
                          artwork: artworkUrl,
                          collectionPrice: collectionPrice,
                          trackCount: get("trackCount"),
                          copyright: get("copyright") as String? ?? "",
                          currency: get("currency"),
                          releaseDate: releaseDate,
                          genre: get("primaryGenreName"),
                          explicitness: Explicitness(rawValue: (get("collectionExplicitness") as String)) ?? Explicitness.notExplicit)

    return album
}

fileprivate func parseAlbums(fromJson jsonObject: Any) throws -> [AlbumInfo] {
    guard let root = jsonObject as? [String: Any],
        let results = root["results"] as? [Any]
        else { throw MissedPropertyError("results") }

    return try results.map(parseAlbum(fromJson: ))
}

fileprivate func parseTrack(fromJson jsonObject: Any) throws -> TrackInfo? {
    guard let dict = jsonObject as? [String: Any] else { throw MissedPropertyError("result item") }
    guard let wrapperType = dict["wrapperType"] as? String else { throw MissedPropertyError("wrapperType") }

    // results contain not only track but collection (album) too
    guard wrapperType == "track" else { return nil }

    func get<T>(_ propertyName: String) throws -> T {
        let result = dict[propertyName] as Any
        guard let value = result as? T else { throw MissedPropertyError(propertyName) }
        return value
    }

    let track = try TrackInfo(trackName: get("trackName"),
                              previewUrl: (get("previewUrl") as String?).flatMap(URL.init(string: )),
                              explicitness: Explicitness(rawValue: (get("trackExplicitness") as String)) ?? Explicitness.notExplicit,
                              disk: get("discNumber"),
                              number: get("trackNumber"),
                              genre: get("primaryGenreName"))

    return track
}

fileprivate func parseTracks(fromJson jsonObject: Any) throws -> [TrackInfo] {
    guard let root = jsonObject as? [String: Any],
        let results = root["results"] as? [Any]
        else { throw MissedPropertyError("results") }

    return try results.compactMap(parseTrack(fromJson: ))
}
