//
//  SearchView.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 13.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import RxSwift

protocol SearchView: class {
    associatedtype SearchResultView
    associatedtype FastLookupResultView
    associatedtype SearchResultIndex

    func reload(recordCount: Int)

    var searchQuery: Observable<String> { get }

    var itemSelected: Observable<SearchResultIndex> { get }

    var updateFastLookupResultViewRequest: Observable<(FastLookupResultView, SearchResultIndex)> { get }
    var updateResultViewRequest: Observable<(SearchResultView, SearchResultIndex)> { get }
}
