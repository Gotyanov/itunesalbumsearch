//
//  SubscriptionsOwner.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 14.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import RxSwift

protocol SubscriptionsOwner {
    var disposeBag: DisposeBag { get }
}

protocol ClearableSubscriptionsOwner: SubscriptionsOwner {
    func removeAllSubscriptions()
}
