//
//  AppDelegate.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 13.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var wireframeDisposeBag: DisposeBag?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        guard let navigationController = window?.rootViewController as? UINavigationController
            else { fatalError("root controller is not a navigation controller") }

        guard let albumCollectionViewController =
            navigationController.viewControllers.first as? AlbumCollectionViewController
            else { fatalError("Can't get \(AlbumCollectionViewController.self)") }

        let albumClient = ITunesClient()

        wireframeDisposeBag = createWireframe(
            albumClient: albumClient,
            albumCollectionViewController: albumCollectionViewController,
            rootNavController: navigationController)

        return true
    }
}
