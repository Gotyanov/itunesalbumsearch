//
//  AlbumInfo.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 13.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation

struct AlbumInfo {
    var artistID: Int
    var collectionID: Int
    var artistName: String
    var collectionName: String
    var artistViewUrl: URL?
    var collectionViewUrl: URL
    var artwork: URL?
    var collectionPrice: Decimal?
    var trackCount: Int
    var copyright: String
    var currency: String
    var releaseDate: Date
    var genre: String
    var explicitness: Explicitness
}
