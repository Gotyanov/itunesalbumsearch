//
//  TrackInfo.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 13.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation

enum Explicitness: String {
    case explicit
    case cleaned
    case notExplicit
}

struct TrackInfo {
    var trackName: String
    var previewUrl: URL?
    var explicitness: Explicitness
    var disk: Int
    var number: Int
    var genre: String
}
