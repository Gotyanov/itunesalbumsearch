//
//  LoadableView.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 16.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import RxSwift

protocol LoadableView {
    var loaded: Observable<Bool> { get }
}
