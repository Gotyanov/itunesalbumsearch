//
//  AlbumCollectionViewController.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 14.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit
import RxSwift

private let reuseIdentifier = "AlbumCell"

final class AlbumCollectionViewController: UICollectionViewController {
    typealias SearchResultView = AlbumCollectionViewCell
    typealias FastLookupResultView = FastLookupSearchResultCell
    typealias SearchResultIndex = Int

    var fastLookupViewController = FastLookupSearchResultViewController()
    var searchController: UISearchController!
    
    fileprivate let updateResultViewRequestSubject = PublishSubject<(AlbumCollectionViewCell, Int)>()
    fileprivate let itemSelectedSubject = PublishSubject<SearchResultIndex>()
    fileprivate let searchQuerySubject = BehaviorSubject(value: "")
    fileprivate let inLookupModeSubject = BehaviorSubject(value: false)
    fileprivate let recordsCountSubject = BehaviorSubject(value: 0)

    private var recordCount = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView!.register(UINib(nibName: "\(AlbumCollectionViewCell.self)", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)

        let flowLayout = collectionView!.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.itemSize = CGSize(width: view.bounds.width, height: 70)

        searchController = UISearchController(searchResultsController: fastLookupViewController)
        searchController.searchResultsUpdater = self
        searchController.delegate = self
        searchController.searchBar.delegate = self

        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false

        definesPresentationContext = true

        _ = Observable.combineLatest(inLookupModeSubject, recordsCountSubject).subscribe(onNext: { [weak self] (inLookupMode, recordCount) in
            switch inLookupMode {
            case true:
                self?.fastLookupViewController.setRecordCount(recordCount: recordCount)
                self?.setRecordCount(recordCount: 0)
            case false:
                self?.fastLookupViewController.setRecordCount(recordCount: 0)
                self?.setRecordCount(recordCount: recordCount)
            }
        })
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recordCount
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! SearchResultView
        updateResultViewRequestSubject.onNext((cell, indexPath.row))
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        itemSelectedSubject.onNext(indexPath.item)
    }
}

extension AlbumCollectionViewController: SearchView {
    func reload(recordCount: Int) {
        dispatchPrecondition(condition: .onQueue(.main))
        recordsCountSubject.onNext(recordCount)
    }

    func setRecordCount(recordCount: Int) {
        self.recordCount = recordCount
        collectionView?.reloadData()
    }

    var searchQuery: Observable<String> {
        return searchQuerySubject
    }

    var updateFastLookupResultViewRequest: Observable<(FastLookupSearchResultCell, Int)> {
        return fastLookupViewController.updateFastLookupResultViewRequestSubject
    }

    var updateResultViewRequest: Observable<(AlbumCollectionViewCell, Int)> {
        return updateResultViewRequestSubject
    }

    var itemSelected: Observable<SearchResultIndex> {
        return Observable.merge([itemSelectedSubject, fastLookupViewController.itemSelectedSubject]) 
    }
}

extension AlbumCollectionViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        searchQuerySubject.onNext(searchController.searchBar.text ?? "")
    }
}

extension AlbumCollectionViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchController.dismiss(animated: true, completion: nil)
        inLookupModeSubject.onNext(false)
    }
}

extension AlbumCollectionViewController: UISearchControllerDelegate {
    func willPresentSearchController(_ searchController: UISearchController) {
        inLookupModeSubject.onNext(true)
    }

    func willDismissSearchController(_ searchController: UISearchController) {
        inLookupModeSubject.onNext(false)
    }
}
