//
//  AlbumCell.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 14.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit
import RxSwift

final class AlbumCollectionViewCell: UICollectionViewCell {

    static let defaultImage = #imageLiteral(resourceName: "Library Icon")

    var disposeBag = DisposeBag()

    @IBOutlet var albumNameLabel: UILabel!
    @IBOutlet var artistNameLabel: UILabel!
    @IBOutlet var yearLabel: UILabel!
    @IBOutlet var explicitContentLabel: UILabel!
    @IBOutlet var artworkImageView: UIImageView!

}

extension AlbumCollectionViewCell: AlbumView {

    var albumName: String {
        get {
            return albumNameLabel.text ?? ""
        }
        set {
            albumNameLabel.text = newValue
        }
    }

    var artistName: String {
        get {
            return artistNameLabel.text ?? ""
        }
        set {
            artistNameLabel.text = newValue
        }
    }

    var artwork: UIImage? {
        get {
            return artworkImageView.image
        }
        set {
            artworkImageView.image = newValue ?? AlbumCollectionViewCell.defaultImage
        }
    }

    var explicitContent: Bool {
        get {
            return !explicitContentLabel.isHidden
        }
        set {
            explicitContentLabel.isHidden = !newValue
        }
    }

    var year: String {
        get {
            return yearLabel.text ?? ""
        }
        set {
            yearLabel.text = newValue
        }
    }
}

extension AlbumCollectionViewCell: ClearableSubscriptionsOwner {
    func removeAllSubscriptions() {
        disposeBag = DisposeBag()
    }
}
