//
//  ParsingError.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 13.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

struct MissedPropertyError: Error, CustomStringConvertible, CustomDebugStringConvertible {
    let propertyName: String

    init(_ property: String) {
        propertyName = property
    }

    var description: String {
        return "ParsingError: " + propertyName
    }

    var debugDescription: String {
        return description
    }
}

struct NotCorrectUrlProperty: Error, CustomStringConvertible {
    let propertyName: String

    init(_ property: String) {
        propertyName = property
    }

    var description: String {
        return "NotCorrectUrlProperty: " + propertyName
    }
}
