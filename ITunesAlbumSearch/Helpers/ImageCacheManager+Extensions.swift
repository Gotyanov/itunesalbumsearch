//
//  ImageCacheManager+Extensions.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 14.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import RxSwift

extension ImageCacheManager {
    func getOrFetch(url: URL) -> Observable<UIImage?> {
        if let cachedImage = self.cachedImage(url: url) {
            return Observable.of(cachedImage)
        }

        return Observable.create { observer in
            self.fetchImage(url: url, completion: { image in
                observer.onNext(image)
                observer.onCompleted()
            })

            return Disposables.create()
        }.share()
    }
}
