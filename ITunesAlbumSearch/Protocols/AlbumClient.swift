//
//  AlbumClient.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 13.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import RxSwift

protocol AlbumClient {
    func findAlbum(query: String) -> Observable<[AlbumInfo]>
    func getTracks(forAlbum album: Int) -> Observable<[TrackInfo]>
}
