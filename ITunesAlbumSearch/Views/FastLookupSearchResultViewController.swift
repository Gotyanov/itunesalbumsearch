//
//  FastLookupSearchResultViewController.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 14.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit
import RxSwift

private let reuseIdentifier = "FastLookupCell"

final class FastLookupSearchResultViewController: UITableViewController {
    var recordCount = 0
    internal let updateFastLookupResultViewRequestSubject = PublishSubject<(FastLookupSearchResultCell, Int)>()
    internal let itemSelectedSubject = PublishSubject<Int>()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(FastLookupSearchResultCell.self, forCellReuseIdentifier: reuseIdentifier)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordCount
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! FastLookupSearchResultCell
        updateFastLookupResultViewRequestSubject.onNext((cell, indexPath.row))
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        itemSelectedSubject.onNext(indexPath.row)
    }

    func setRecordCount(recordCount: Int) {
        self.recordCount = recordCount
        tableView.reloadData()
    }
}

final class FastLookupSearchResultCell: UITableViewCell {
    override init(style _: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: reuseIdentifier)
    }

    required init?(coder aDecoder: NSCoder) {
        return nil
    }
}

extension FastLookupSearchResultCell: AlbumFastLookupView {
    var albumName: String {
        get {
            return textLabel?.text ?? ""
        }
        set {
            textLabel?.text = newValue
        }
    }

    var artistName: String {
        get {
            return detailTextLabel?.text ?? ""
        }
        set {
            detailTextLabel?.text = newValue
        }
    }
}
