//
//  AlbumView.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 14.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit

protocol AlbumFastLookupView: class {
    var albumName: String { get set }
    var artistName: String { get set }
}

protocol AlbumView: AlbumFastLookupView {
    var artwork: UIImage? { get set }
    var explicitContent: Bool { get set }
    var year: String { get set }
}
