//
//  LoadingObject.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 15.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

enum LoadingObject<Payload> {
    case loading
    case loaded(Payload)
}
