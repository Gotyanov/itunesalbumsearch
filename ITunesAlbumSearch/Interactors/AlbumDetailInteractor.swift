//
//  TrackDetailInteractor.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 16.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import RxSwift
import UIKit

final class AlbumDetailInteractor {
    let disposeBag = DisposeBag()
    let view: UIViewController
    var errorObservable = PublishSubject<Error>()

    init(view: UIViewController) {
        self.view = view
    }
}

extension AlbumDetailView where Self: UIViewController & LoadableView, Self.TrackView: ITunesAlbumSearch.TrackView, TrackViewIndex == Int {
    func createInteractor<Client: AlbumClient>(albumDetailObservable: Observable<AlbumInfo>, client: Client, imageCacheManager: ImageCacheManager) -> AlbumDetailInteractor {
        let interactor = AlbumDetailInteractor(view: self)

        let albumsAfterViewLoadingSequence = Observable.combineLatest(
            loaded.filter({ $0 }),
            albumDetailObservable,
            resultSelector: { _, album in album })

        albumsAfterViewLoadingSequence
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { album in

                self.albumName = album.collectionName
                self.artistName = album.artistName
                self.genre = album.genre
                self.trackCount = formatTrackCount(album.trackCount)
                self.releaseDate = formatDate(album.releaseDate)
                if let price = album.collectionPrice {
                    self.price = formatPrice(price: price, currency: album.currency)
                } else {
                    self.price = "Preorder"
                }

                self.explicitContent = album.explicitness == .explicit
                self.copyright = album.copyright

                self.artwork = nil
                let loadImage = album.artwork.map {
                    imageCacheManager.getOrFetch(url: $0)
                    } ?? Observable.of(nil)
                loadImage.subscribe(onNext: { self.artwork = $0 }).disposed(by: interactor.disposeBag)
                
            }).disposed(by: interactor.disposeBag)

        albumDetailObservable.flatMapLatest { album in self.tapOnArtistName.map { _ in album} }
            .subscribe(onNext: { album in
                guard let artistUrl = album.artistViewUrl else { return }
                UIApplication.shared.open(artistUrl, options: [:])
            }).disposed(by: interactor.disposeBag)

        let tracksSequence = albumsAfterViewLoadingSequence.flatMapLatest { album in
            client.getTracks(forAlbum: album.collectionID)
                .map({ LoadingObject.loaded($0.sorted(by: sortTracks)) })
                .startWith(.loading)
            }
            .do(onError: interactor.errorObservable.onError)
            .catchErrorJustReturn(.loaded([]))


        var tracks: [TrackInfo] = []
        tracksSequence
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { loadingTracks in
                switch loadingTracks {
                case .loading:
                    tracks = []
                    self.setTrackCount(.loading)
                case .loaded(let t):
                    tracks = t
                    self.setTrackCount(.loaded(t.count))
                }
            }).disposed(by: interactor.disposeBag)

        self.updateTrackViewRequest
            .subscribe(onNext: { (view, index) in
                configureTrackView(view: view, index: index, trackInfo: tracks[index])
            }).disposed(by: interactor.disposeBag)

        return interactor
    }
}

private func formatTrackCount(_ count: Int) -> String {
    switch count {
    case 0: return "no"
    case 1: return "1 track"
    default: return "\(count) tracks"
    }
}

private func formatDate(_ date: Date) -> String {
    struct Store {
        static let formatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            formatter.timeStyle = .none
            return formatter
        }()
    }

    return Store.formatter.string(from: date)
}

private func formatPrice(price: Decimal, currency: String) -> String {
    let locale = Locale.current
    guard locale.currencyCode?.lowercased() == currency.lowercased() else {
        return "\(price) \(currency)"
    }

    let formatter = NumberFormatter()
    formatter.numberStyle = .currency
    return formatter.string(from: price as NSNumber)!
}

private func sortTracks(t1: TrackInfo, t2: TrackInfo) -> Bool {
    return (t1.disk, t1.number) < (t2.disk, t2.number)
}

private func configureTrackView<View: TrackView>(view: View, index: Int, trackInfo: TrackInfo) {
    view.orderNumber = (index + 1).description
    view.trackName = trackInfo.trackName
    view.explicitContent = trackInfo.explicitness == .explicit
}
