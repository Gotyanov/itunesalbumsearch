//
//  AlbumListInteractor.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 14.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import Foundation
import RxSwift

final class AlbumListInteractor: SubscriptionsOwner {
    var findedAlbums: [AlbumInfo] = []
    let disposeBag = DisposeBag()
    let imageCacheManager = ImageCacheManager()
    fileprivate let openAlbumDetailsSubject = PublishSubject<AlbumInfo>()
    var openAlbumDetailsRequest: Observable<AlbumInfo> {
        return openAlbumDetailsSubject
    }

    var errorObservable = PublishSubject<Error>()
}

func configureAlbumView<View: AlbumFastLookupView>(albumFastLookupView albumView: View, album: AlbumInfo) {
    albumView.albumName = album.collectionName
    albumView.artistName = album.artistName
}

func configureAlbumView<View: AlbumView & ClearableSubscriptionsOwner>(albumView: View, album: AlbumInfo, loadImage: Observable<UIImage?>) {
    configureAlbumView(albumFastLookupView: albumView, album: album)

    albumView.removeAllSubscriptions()
    albumView.explicitContent = album.explicitness == .explicit
    albumView.year = Calendar.current.component(.year, from: album.releaseDate).description
    albumView.artwork = nil
    loadImage.subscribe(onNext: { albumView.artwork = $0 }).disposed(by: albumView.disposeBag)
}

extension SearchView where SearchResultView: AlbumView & ClearableSubscriptionsOwner, FastLookupResultView: AlbumFastLookupView, SearchResultIndex == Int {
    func createInteractor<Client: AlbumClient>(client: Client) -> AlbumListInteractor {

        let interactor = AlbumListInteractor()

        itemSelected.map { index in interactor.findedAlbums[index] }
            .subscribe(interactor.openAlbumDetailsSubject)
            .disposed(by: interactor.disposeBag)

        let disposeBag = interactor.disposeBag

        func updateResults(albums: [AlbumInfo]) {
            interactor.findedAlbums = albums
            reload(recordCount: albums.count)
        }

        func updateAlbumView(albumView: SearchResultView, albumIndex: Int) {
            let album = interactor.findedAlbums[albumIndex]
            let loadImage = album.artwork.map {
                interactor.imageCacheManager.getOrFetch(url: $0)
            } ?? Observable.of(nil)

            configureAlbumView(albumView: albumView, album: album, loadImage: loadImage)
        }

        func updateAlbumView(albumLookupView: FastLookupResultView, albumIndex: Int) {
            let album = interactor.findedAlbums[albumIndex]
            configureAlbumView(albumFastLookupView: albumLookupView, album: album)
        }

        func handleError(error: Error) {
            interactor.errorObservable.onNext(error)
        }

        searchQuery.throttledUserInput
            .flatMapLatest(client.findAlbum(query:))
            .observeOn(MainScheduler.asyncInstance)
            .subscribe(onNext: updateResults, onError: handleError)
            .disposed(by: disposeBag)

        updateResultViewRequest.subscribe(onNext: updateAlbumView)
            .disposed(by: disposeBag)

        updateFastLookupResultViewRequest.subscribe(onNext: updateAlbumView)
            .disposed(by: disposeBag)

        return interactor
    }
}
