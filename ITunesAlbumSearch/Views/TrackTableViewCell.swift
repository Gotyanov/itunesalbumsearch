//
//  TrackTableViewCell.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 16.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit

final class TrackTableViewCell: UITableViewCell {

    @IBOutlet var orderLabel: UILabel!
    @IBOutlet var trackNameLabel: UILabel!
    @IBOutlet var explicitContentLabel: UILabel!

}

extension TrackTableViewCell: TrackView {
    var orderNumber: String {
        get {
            return orderLabel.text ?? ""
        }
        set {
            orderLabel.text = newValue
        }
    }

    var trackName: String {
        get {
            return trackNameLabel.text ?? ""
        }
        set {
            trackNameLabel.text = newValue
        }
    }

    var explicitContent: Bool {
        get {
            return !explicitContentLabel.isHidden
        }
        set {
            explicitContentLabel.isHidden = !newValue
        }
    }
}
