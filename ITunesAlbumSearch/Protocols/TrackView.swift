//
//  TrackView.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 15.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

protocol TrackView: class {
    var orderNumber: String { get set }
    var trackName: String { get set }
    var explicitContent: Bool { get set }
}
