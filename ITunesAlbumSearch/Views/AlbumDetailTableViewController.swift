//
//  AlbumDetailTableViewController.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 14.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import UIKit
import RxSwift

fileprivate enum CellIDs: String {
    case loading = "LoadingTableViewCell"
    case track = "TrackTableViewCell"
}

final class AlbumDetailTableViewController: UITableViewController {
    typealias TrackView = TrackTableViewCell
    typealias TrackViewIndex = Int

    static let defaultImage = #imageLiteral(resourceName: "Library Icon")

    @IBOutlet var albumNameLabel: UILabel!
    @IBOutlet var artistNameButton: UIButton!
    @IBOutlet var releaseDateLabel: UILabel!
    @IBOutlet var explicitContentLabel: UILabel!
    @IBOutlet var artworkImageView: UIImageView!
    @IBOutlet var genreLabel: UILabel!
    @IBOutlet var trackCountLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var copyrightLabel: UILabel!

    fileprivate let tapOnArtistSubject = PublishSubject<Void>()
    fileprivate let loadedSubject = BehaviorSubject(value: false)
    fileprivate let updateTrackViewSubject = PublishSubject<(TrackTableViewCell, Int)>()

    fileprivate var tracksAreLoaded = false
    fileprivate var loadedTrackCount = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.largeTitleDisplayMode = .never

        do {
            priceLabel.textColor = view.tintColor
            let priceLabelContainer = priceLabel.superview!
            priceLabelContainer.layer.cornerRadius = 3
            priceLabelContainer.layer.borderColor = view.tintColor.cgColor
            priceLabelContainer.layer.borderWidth = 1
        }

        loadedSubject.onNext(true)
    }

    @IBAction func tapOnArtist() {
        tapOnArtistSubject.onNext(())
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section, tracksAreLoaded) {
        case (0, true): return 0
        case (0, false): return 1
        case (1, true): return loadedTrackCount
        case (1, false): return 0
        default: fatalError("Impossible section number")
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            return tableView.dequeueReusableCell(withIdentifier: CellIDs.loading.rawValue, for: indexPath)
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIDs.track.rawValue, for: indexPath) as? TrackTableViewCell
                else { fatalError("Wrong table cell type. Should be \(TrackTableViewCell.self)") }
            updateTrackViewSubject.onNext((cell, indexPath.row))
            return cell
        default: fatalError("Impossible section number")
        }
    }
}

extension AlbumDetailTableViewController: AlbumDetailView {
    var albumName: String {
        get {
            return albumNameLabel.text ?? ""
        }
        set {
            albumNameLabel.text = newValue
        }
    }

    var artistName: String {
        get {
            return artistNameButton.title(for: .normal) ?? ""
        }
        set {
            artistNameButton.setTitle(newValue, for: .normal)
        }
    }

    var genre: String {
        get {
            return genreLabel.text ?? ""
        } set {
            genreLabel.text = newValue
        }
    }

    var trackCount: String {
        get {
            return trackCountLabel.text ?? ""
        }
        set {
            trackCountLabel.text = newValue
        }
    }

    var releaseDate: String {
        get {
            return releaseDateLabel.text ?? ""
        }
        set {
            releaseDateLabel.text = newValue
        }
    }

    var price: String {
        get {
            return priceLabel.text ?? ""
        }
        set {
            priceLabel.text = newValue
        }
    }

    var copyright: String {
        get {
            return copyrightLabel.text ?? ""
        }
        set {
            copyrightLabel.text = newValue
        }
    }

    var explicitContent: Bool {
        get {
            return !explicitContentLabel.isHidden
        }
        set {
            explicitContentLabel.isHidden = !newValue
        }
    }

    var tapOnArtistName: Observable<Void> {
        return tapOnArtistSubject
    }

    var artwork: UIImage? {
        get {
            return artworkImageView.image
        }
        set {
            artworkImageView.image = newValue ?? AlbumDetailTableViewController.defaultImage
        }
    }

    func setTrackCount(_ count: LoadingObject<Int>) {
        switch count {
        case .loading:
            tracksAreLoaded = false
        case .loaded(let count):
            tracksAreLoaded = true
            loadedTrackCount = count
        }

        tableView.reloadData()
    }

    var updateTrackViewRequest: Observable<(TrackTableViewCell, Int)> {
        return updateTrackViewSubject
    }
}

extension AlbumDetailTableViewController: LoadableView {
    var loaded: Observable<Bool> {
        return loadedSubject
    }
}
