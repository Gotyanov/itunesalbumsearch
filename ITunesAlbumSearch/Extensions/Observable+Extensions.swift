//
//  Observable+Extensions.swift
//  ITunesAlbumSearch
//
//  Created by Aleksey Gotyanov on 14.04.2018.
//  Copyright © 2018 Aleksey Gotyanov. All rights reserved.
//

import RxSwift

private let USER_DELAY_INPUT = 0.3

extension Observable where Element == String {
    var throttledUserInput: Observable<Element> {
        return throttle(USER_DELAY_INPUT, scheduler: MainScheduler.instance).distinctUntilChanged()
    }
}
